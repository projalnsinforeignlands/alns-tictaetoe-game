package de.com.alns.jobstests.upworkbicos;

import java.util.Scanner;

public class TicTacToeALNSGame {

    public static final int GRID = 3;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Game game;
        loop:
        while (true) {
        	
        	try {
        	
        		System.out.println("==> Enter the Board's Size: ");

        		String boardsSize = scanner.next();
                
        		System.out.println("==> Enter number of human players: ");

        		String qtyPlayers = scanner.next();
                
        		switch (Integer.parseInt(qtyPlayers)) {
                    case 1:
                        game = new Game("Player1", Integer.parseInt(boardsSize));
                        break loop;
                    case 2:
                        game = new Game("Player1", "Player2", Integer.parseInt(boardsSize));
                        break loop;
                }
            } catch (Exception e) {
            }
        }
        game.start();
        Player winner = game.getWinner();
        System.out.println(winner != null ? "Winner is " + winner.getName() : "Tied");
    }


}
